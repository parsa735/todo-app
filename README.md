# Todo App

## Getting Started

- Run `yarn deps` in the root of the project to install dependencies.
- Run `yarn dev` inside the client folder and `yarn dev` inside the server folder to start the servers

## Tasks

- Run `yarn format` to format all the files with Prettier.

## Todo

- [ ] Add a [toaster](https://www.npmjs.com/package/@ajmey/toaster) to indicate when a todo is created, deleted, etc
