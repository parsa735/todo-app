import React, { useState } from "react";

const TodoItem = props => {
  const [todo, setTodo] = useState({
    text: props.todo.text,
    completed: props.todo.completed
  });

  const updateTodo = (key, value) => setTodo(p => ({ ...p, [key]: value }));

  return (
    <div className="todo-item">
      <input
        type="text"
        value={todo.text}
        className={`${props.todo.completed ? "todo-done" : ""}`}
        onChange={event => updateTodo("text", event.target.value)}
      />
      <button onClick={_ => props.deleteTodo(props.todo._id)}>
        <i className="fa fa-trash"></i>
      </button>
      <button
        onClick={_ => props.completeTodo(props.todo._id, !props.todo.completed)}
      >
        <i className="fa fa-check-circle"></i>
      </button>
      <button onClick={_ => props.updateTodoText(props.todo._id, todo.text)}>
        <i className="fa fa-check"></i>
      </button>
    </div>
  );
};

export default TodoItem;
