import "./new-todo.css";
import React, { useState } from "react";
import client from "../../common/client";
const todosSvc = client.service("todos");

const initialState = { text: "", completed: false };

const NewTodo = props => {
  const [todo, setTodo] = useState(initialState);

  const updateTodoState = (key, value) =>
    setTodo(p => ({ ...p, [key]: value }));

  const createTodo = async () => {
    try {
      await todosSvc.create(todo);
      window.toaster.success("Todo was created");
      setTodo(initialState);
    } catch (err) {
      window.toaster.failure(err.message);
    }
  };

  return (
    <div className="input-plus">
      <input
        type="text"
        placeholder="Enter text"
        value={todo.text}
        className="form-control"
        onChange={event => updateTodoState("text", event.target.value)}
      />
      <i className="fa fa-plus-circle" onClick={createTodo}></i>
    </div>
  );
};

export default NewTodo;
