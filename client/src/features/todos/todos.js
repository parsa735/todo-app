import "./todos.css";
import React, { useEffect, useState } from "react";
import client from "../../common/client";
import NewTodo from "./new-todo";
import TodoItem from "./todo-item";
const todosSvc = client.service("todos");

const Todos = props => {
  const [todos, setTodos] = useState([]);

  const refreshTodos = () => {
    todosSvc.find({ query: { $limit: 10000 } }).then(res => {
      setTodos(res.data);
    });
  };

  /* Get all the todos on component load */
  useEffect(() => {
    refreshTodos();
  }, []);

  /* Subscribe to todos CRUD events, created, patched, removed
   * because of this, we won't have to worry about fetching todos again
   * every time after any change to todos.
   */
  useEffect(() => {
    const events = ["created", "patched", "removed"];
    events.forEach(event => {
      todosSvc.on(event, d => {
        refreshTodos();
      });
    });

    return () => {
      events.forEach(event => todosSvc.off(event));
    };
  }, []);

  const updateTodoText = async (id, text) => {
    try {
      await todosSvc.patch(id, { text });
      window.toaster.success("Todo was updated");
    } catch (err) {
      window.toaster.failure(err.message);
    }
  };

  const completeTodo = async (id, completed) => {
    try {
      await todosSvc.patch(id, { completed });
      window.toaster.success("Todo completed");
    } catch (err) {
      window.toaster.failure(err.message);
    }
  };

  const deleteTodo = async id => {
    try {
      await todosSvc.remove(id);
      window.toaster.success("Todo was deleted");
    } catch (err) {
      window.toaster.failure(err.message);
    }
  };

  return (
    <div>
      <NewTodo />
      <ol>
        {todos.map(todo => {
          return (
            <li key={todo._id}>
              <TodoItem
                completeTodo={completeTodo}
                deleteTodo={deleteTodo}
                updateTodoText={updateTodoText}
                todo={todo}
              />
            </li>
          );
        })}
      </ol>
    </div>
  );
};

export default Todos;
