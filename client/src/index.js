import React from "react";
import ReactDOM from "react-dom";
import App from "./application";
import * as serviceWorker from "./worker";
import "bootstrap/dist/css/bootstrap.css";
import "@ajmey/toaster/themes/theme-default.min.css";
import { useToaster } from "@ajmey/toaster/toaster";
window.toaster = useToaster({ animation: "appear", theme: "default" });

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
