import React, { Fragment } from "react";
import Todos from "./features/todos/todos";

const App = props => {
  return (
    <Fragment>
      <Todos />
    </Fragment>
  );
};

export default App;
